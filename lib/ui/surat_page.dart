import 'package:flutter/material.dart';
import 'package:aplikasi_quran_uas/common/contants.dart';
import 'package:aplikasi_quran_uas/cubit/surat/surat_cubit.dart';
import 'package:aplikasi_quran_uas/ui/ayat_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SuratPage extends StatefulWidget {
  const SuratPage({super.key});

  @override
  State<SuratPage> createState() => _SuratPageState();
}

class _SuratPageState extends State<SuratPage> {
  @override
  void initState() {
    context.read<SuratCubit>().getAllSurat();
    super.initState(); // untuk blocbuildernya agar kepanggil lalu masuk ke bloc provider di main
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('All Surat')),
      body: BlocBuilder<SuratCubit, SuratState>(
        builder: (context, state) {
          if (state is SuratLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is SuratLoaded) { // kalau surat lagi loading maka
            return ListView.builder(
              itemBuilder: (context, index) {
                final surat = state.listSurat[index];
                return InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                          return AyatPage(surat: surat);
                        }));
                  },
                  child: Card(
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundColor: AppColors.primary,
                        child: Text(
                          '${surat.nomor}',
                          style: const TextStyle(
                            color: AppColors.white,
                          ),
                        ),
                      ),
                      title: Text('${surat.namaLatin}, ${surat.nama}'),
                      subtitle:
                      Text('${surat.arti}, ${surat.jumlahAyat} Ayat.'),
                    ),
                  ),
                );
              },
              itemCount: state.listSurat.length,
            );
          }

          if (state is SuratError) { // ini kalau error dia ngapain? maka akan return message eeor
            return Center(
              child: Text(state.message),
            );
          }

          return const Center( //ini default kembalian
            child: Text('no data'),
          );
        },
      ),
    );
  }
}