import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:aplikasi_quran_uas/data/models/surat_detail_model.dart';
import 'package:aplikasi_quran_uas/data/models/surat_model.dart';
import 'package:http/http.dart' as http;

class ApiService {
  final http.Client client;
  ApiService({
    required this.client,
  });

  //errornya string , benarnya list
  Future<Either<String, List<SuratModel>>> getAllSurat() async { //mengembalikan suksesnya apa dan errornya apa
    try {
      final response =
      await client.get(Uri.parse('https://equran.id/api/surat'));
      return Right(List<SuratModel>.from( // surat model bentuknya object
        jsonDecode(response.body).map( //jsonDecode => json menjadi object
              (x) => SuratModel.fromJson(x), //trs di map dikeluarin
        ),
      ).toList());
    } catch (e) { // try catch guna mengecek error
      return Left(e.toString());
    }
  }

  Future<Either<String, SuratDetailModel>> getDetailSurat(
      int nomorSurat) async {
    try {
      final response = await client
          .get(Uri.parse('https://equran.id/api/surat/$nomorSurat'));
      return Right(SuratDetailModel.fromJson(jsonDecode(response.body))); //kembaliannya
    } catch (e) {
      return Left(e.toString());
    }
  }
}